Receive data that accurately and completely describe how Internet users use his site. Different aspects 
of web site use are measured using various metrics and indicators. The results 
are presented in the form of statistics ranging from simple statistics, such as the number of Page Views on a given section of 
the web site, to more complex analysis, like technical analysis of individual Internet user’s technical capabilities.

Extractor allows:
- Download daily [Sampled Reports]((https://prism.gemius.com/api/docs/#tag/sampledreports)) 
including [trends](https://prism.gemius.com/api/docs/#operation/getSampledReportTrend)
- Download specific [Monitoring Reports](https://prism.gemius.com/api/docs/#operation/getMonitoringReportTree)
- Get all available [project](https://prism.gemius.com/api/docs/#tag/projects) metadata