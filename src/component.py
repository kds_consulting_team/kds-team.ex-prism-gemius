'''
Created on 15. 04. 2019

@author: esner
'''
import logging
import os
from datetime import datetime
from datetime import timedelta

import pytz
from kbc.env_handler import KBCEnvHandler
from kbc.result import KBCTableDef
from kbc.result import ResultWriter

from gemius.prism_ex_service import PrismExService

KEY_BACKFILL_WINDOW = 'backfill_max_window'

KEY_BACKFILL_ENABLED = 'backfill_enabled'

KEY_REPORT_ID = 'report_id'

KEY_PROJECT_IDS = 'project_ids'
KEY_API_TOKEN = '#api_token'

KEY_INCLUDE_TRENDS = 'include_trends'

KEY_SAMPLED_REPORT = 'sampled_reports'
KEY_OFFLINE_REPORTS = 'offline_reports'
KEY_MONITORING_REPORTS = 'monitoring_reports'

KEY_PERIOD = 'period'
KEY_PERIOD_FROM = 'from_date'
KEY_PERIOD_TO = 'to_date'

KEY_STRUCTURE_ID = 'structure_id'
KEY_VSENDER_TYPE = 'vsender_type'
KEY_METRICS = 'metrics'
KEY_CUSTOM_METRICS = 'custom_metrics'

KEY_BACKFILL_MODE = 'backfill_mode'

KEY_NODE_FILTERS = 'node_filters'

MANDATORY_PARS = [KEY_PROJECT_IDS, KEY_API_TOKEN]

APP_VERSION = '0.0.7'


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS)
        # override debug from config
        if self.cfg_params.get('debug'):
            debug = True

        self.set_default_logger('DEBUG' if debug else 'INFO')
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config()
        except ValueError as e:
            logging.error(e)
            exit(1)

        # intialize instance parameteres
        self.prism_srv = PrismExService(self.cfg_params[KEY_API_TOKEN])

    def run(self, debug=False):
        '''
        Main execution code
        '''

        params = self.cfg_params

        # get projects
        all_projects = self._get_n_store_projects()

        if self.cfg_params.get(KEY_PROJECT_IDS, []):
            project_ids = self.cfg_params[KEY_PROJECT_IDS]
        else:
            project_ids = all_projects

        result_files = []
        # ####### iterate through all projects
        for project_id in project_ids:

            # get sampled reports
            if len(params.get(KEY_SAMPLED_REPORT, [])) > 0:
                # validate params
                self._validate_sample_report_params(params[KEY_SAMPLED_REPORT][0])

                periods, from_date, to_date = self._get_sampled_peroid(params[KEY_SAMPLED_REPORT][0])
                # collect results
                rs = self._get_sampled_reports(periods, from_date, to_date, params[KEY_SAMPLED_REPORT][0], project_id)
                result_files.extend(rs)
                # set state to last ran period
                if params[KEY_SAMPLED_REPORT][0].get(KEY_BACKFILL_MODE, {}).get(KEY_BACKFILL_ENABLED):
                    self.write_state_file({'last_period': {'start_date': str(from_date),
                                                           'end_date': str(to_date)}})

            # get offline reports
            if len(params.get(KEY_MONITORING_REPORTS, [])) > 0:
                rs = self._get_monitoring_reports(params[KEY_MONITORING_REPORTS][0], project_id)
                result_files.extend(rs)

            logging.info('Building manifest files..')
            self._process_results_sliced(result_files)

        logging.info('Extraction finished sucessfully!')

    def _process_results(self, res_files, output_bucket):
        for res in res_files:
            dest_bucket = 'in.c-kds-team-ex-prism-gemius-' + \
                          str(self.kbc_config_id)
            if output_bucket:
                suffix = '-' + output_bucket
            else:
                suffix = ''

            # build manifest
            self.configuration.write_table_manifest(
                file_name=res['full_path'],
                destination=dest_bucket + suffix + '.' + res['name'],
                primary_key=res['pkey'],
                incremental=True)

    def _process_results_sliced(self, res_files):
        res_sliced_folders = {}
        for file in res_files:
            res_sliced_folders.update({file['name']: file['pkey']})

        for folder in res_sliced_folders:
            self.create_sliced_tables(folder, res_sliced_folders[folder], True)

    def _get_backfill_period(self, interval, from_date):
        state = self.get_state_file()
        diff = timedelta(days=interval)

        if state and state.get('last_period'):
            last_end_date = datetime.strptime(
                state['last_period']['end_date'], '%Y-%m-%d %H:%M:%S.%f')

            start_date = last_end_date

        else:
            start_date = from_date
            last_end_date = start_date

        if (last_end_date.date() + diff) >= datetime.now(pytz.utc).date() + timedelta(days=1):
            end_date = datetime.now(pytz.utc).replace(tzinfo=None)
        else:
            end_date = last_end_date + diff
        return start_date, end_date

    def _get_sampled_reports(self, periods, from_date, to_date, params, project_id):
        """
        Gets and stores sampled reports results
        :param periods:
        :param from_date:
        :param to_date:
        :param params:
        :return:
        """
        report_req_param = {'structure_id': params[KEY_STRUCTURE_ID],
                            'node_filters': params.get(KEY_NODE_FILTERS),
                            'vsender_type': params[KEY_VSENDER_TYPE],
                            'output_folder_path': os.path.join(self.tables_out_path),
                            'metrics': params.get(KEY_METRICS),
                            'custom_metrics': params.get(KEY_CUSTOM_METRICS),
                            'include_trends': params.get(KEY_INCLUDE_TRENDS, False),
                            'project_id': project_id}

        logging.info('Retrieving reports for given period [%s - %s], with filter:%s',
                     str(from_date), str(to_date), str(params.get(KEY_NODE_FILTERS)))

        result_files = []
        for period in periods:
            report_req_param['start_date'] = period['start_date']
            report_req_param['end_date'] = period['end_date']

            result_files += self.prism_srv.get_n_save_sampled_report_node_tree(**report_req_param)
        return result_files

    def _get_n_store_projects(self):
        tb_def = KBCTableDef('projects', [], ['id'])
        p_writer = ResultWriter(self.tables_out_path, tb_def)

        logging.info('Getting all projects..')
        res = self.prism_srv.get_projects()
        p_writer.write_all(res)

        logging.info('Building project manifest files.')
        self.create_manifests(p_writer.collect_results())
        # return project ids
        return [p['id'] for p in res]

    def _get_offline_reports(self, param, last_state):
        pass

    def _get_monitoring_reports(self, params, project_id):
        return self.prism_srv.get_n_save_monitoring_report(self.tables_out_path, project_id, params[KEY_REPORT_ID],
                                                           params[KEY_VSENDER_TYPE],
                                                           params[KEY_PERIOD])

    def _get_sampled_peroid(self, params):
        backfill_mode = params.get(KEY_BACKFILL_MODE)
        if backfill_mode and not (
                params.get(KEY_PERIOD_FROM) and params.get(KEY_PERIOD_TO)):
            logging.error(
                'Period from and Period To paremeters must be defined when backfill mode is on!')
            exit(1)

        if not (params.get(KEY_PERIOD_FROM) or params.get(KEY_PERIOD_TO)):
            logging.error(
                'Period from and Period To paremeters must be defined for Sampled Repot mode is on!')
            exit(1)
        from_date, to_date = self.get_date_period_converted(params[KEY_PERIOD_FROM],
                                                            params[KEY_PERIOD_TO])
        # get periods either backfill or standard
        if params.get(KEY_BACKFILL_MODE) and params[KEY_BACKFILL_MODE][KEY_BACKFILL_ENABLED]:
            from_date, to_date = self._get_backfill_period(params[KEY_BACKFILL_MODE][KEY_BACKFILL_WINDOW], from_date)

        periods = self.split_dates_to_chunks(
            from_date, to_date, 0, '%Y-%m-%d')
        return periods, from_date, to_date

    def _validate_sample_report_params(self, params):
        try:
            self.validate_parameters(params, [KEY_VSENDER_TYPE, KEY_PERIOD_TO, KEY_PERIOD_FROM, KEY_STRUCTURE_ID],
                                     'Sampled Report params')
        except ValueError as e:
            logging.error(e)
            exit(1)


"""
        Main entrypoint
"""
if __name__ == "__main__":
    comp = Component()
    comp.run()
