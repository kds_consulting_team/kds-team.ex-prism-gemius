'''
Created on 15. 04. 2019

@author: esner
'''
import json

from kbc.client_base import HttpClientBase

DEFAULT_BASE = 'https://prism.gemius.com/api/projects'

VALID_METRICS = ["NoVt", "TotTm", "NoPV", "NoBc", "TotC", "TotRv", "NoEn",
                 "NoEx", "NoC", "NoVr", "NetSV", "StTm", "AvVpVr",
                 "AvVIntr", "AvVL", "ROI", "TotPr", "AvCt", "AvRv", "CRt",
                 "VrTmAv", "PVTmAv", "VTmAv", "BR", "NoA", "NoRpVr", "EnR", "ExR", "AvNetSVVr",
                 "NoCVr", "CVrRt", "StNoVid", "AvStNoViV", "AvStTmVid", "StRt",
                 "GrossSV", "SpotSV", "NetST", "SpotST", "CNoEv", "CNoUVal", "CNoPV", "CNoA", "CNoVt", "CSuVal"]
'''
Unsupported (not documented) metrics
"VLen", "NoNPvBc","NoNBc","BcV","VrTm","Vt","PVt","NoIntr","TotIntr","FD","FVRt","SVt","SCRt","SD",
"SDRt","SPVt","SAvT", "FDRt", "SEnt", "SC,"FC","FAvTeV", "SAvL","FCRt",
"AvStTmVr","StTmVid"
'''

VALID_MODIFICATORS = ["none", "change", "share", "share_in_total", "change_in_percentage"]

MAX_PAGE_SIZE = 500


class Client(HttpClientBase):

    def __init__(self, token, service_base_url=DEFAULT_BASE):
        base_url = service_base_url + '/'
        auth_header = {'Authorization': 'Bearer ' + token,
                       'Content-Type': 'application/json',
                       'Accept': 'application/json'}
        HttpClientBase.__init__(self, base_url=base_url, default_http_header=auth_header)

    def get_projects(self):
        res = self.get(DEFAULT_BASE)
        return res['accountProjects']

    def get_monitoring_report(self, project_id, report_id, period, vsender_type, metrics=None, custom_metrics=None):
        url = DEFAULT_BASE + '/' + str(project_id) + '/monitoringreports/' + str(report_id) + '/tree/' + period
        params = dict()
        if metrics:
            params['metrics'] = metrics
        if custom_metrics:
            params['customMetrics'] = custom_metrics
        params['vsenderType'] = vsender_type
        return self.get(url, params=params)

    def get_offline_report_list(self, project_id, report_id):
        """
        Return list of Offline Reports
        :param project_id:
        :param report_id:
        :return:
        """
        url = DEFAULT_BASE + '/' + str(project_id) + '/reports/' + str(report_id) + '/offlinereports'
        return self.get(url)

    def _get_offline_report(self, project_id, report_id, offline_report_id, nr_rows, page_nr):
        url = DEFAULT_BASE + '/' + str(project_id) + '/reports/' + str(report_id) + '/offlinereports/' + str(
            offline_report_id) + '/table'
        params = {
            "numberOfRows": nr_rows,
            "pageNumber": page_nr
        }

        return self.get(url, params=params)

    def get_sapled_report_all_pages(self, project_id, structure_id, period, segmentationNodeId, metrics,
                                    vsender_type=None,
                                    custom_metrics=None, page_size=MAX_PAGE_SIZE):
        '''

        Params:
        period -- {"startDate" : "2018-01-01",
                   "endDate" : "2018-01-01",
                   "periodFilter: "all"}
        pagination -- {"pageNumber": 1,
                        "rowsPerPage": <=500}
        metrics -- [{"metric" : ,
                     "modificator" : }]

        '''
        page_nr = 1
        pagination = {"pageNumber": page_nr,
                      "rowsPerPage": page_size}
        # get root node with header and main row info
        res = self._get_sampled_report(project_id, structure_id, period, segmentationNodeId, pagination, metrics,
                                       vsender_type,
                                       custom_metrics)
        res_data = self._parse_sampled_response(res)

        page_nr += 1
        pagination['pageNumber'] = page_nr
        hasNext = len(res['rows']) >= page_size
        # iterate through data pages
        while hasNext:
            res = self._get_sampled_report(project_id, structure_id, period, segmentationNodeId, pagination, metrics,
                                           vsender_type,
                                           custom_metrics)

            hasNext = len(res['rows']) != 0 and len(res['rows']) < page_size
            page_nr += 1
            pagination['pageNumber'] = page_nr
            res_data['data'] += res_data['data'] + res['rows']

        return res_data

    def _parse_sampled_response(self, response):
        parsed = {'mainRow': response['mainRow'],
                  'header': response['definitions'],
                  'data': response['rows']}

        return parsed

    def _get_sampled_report(self, project_id, structure_id, period, segmentationNodeId, pagination, metrics,
                            vsender_type=None,
                            custom_metrics=None):
        '''

        Params:
        period -- {"startDate" : "2018-01-01",
                   "endDate" : "2018-01-01",
                   "periodFilter: "all"}
        pagination -- {"pageNumber": 1,
                        "rowsPerPage": <=500}
        metrics -- [{"metric" : ,
                     "modificator" : }]

        '''
        url = self.base_url + project_id + '/structures/' + str(structure_id) + '/table'
        # url = 'http://ptsv2.com/t/10now-1539777619/post'
        data = {}
        data['period'] = period
        if segmentationNodeId:
            data['segmentationNodeId'] = segmentationNodeId
        if pagination:
            data['pagingAndSortingOptions'] = pagination
        if metrics:
            data['metrics'] = metrics
        if custom_metrics:
            data['customMetrics'] = custom_metrics
        if vsender_type:
            data['vsenderType'] = vsender_type

        return self.post(url=url, data=json.dumps(data))

    def get_sampled_report_trend(self, project_id, structure_id, period, segmentationNodeId, node_label, metric,
                                 period_unit='day', vsender_type=None,
                                 custom_metric=None):
        '''

        Params:
        period -- {"startDate" : "2018-01-01",
                   "endDate" : "2018-01-01",

        metrics -- [{"metric" : ,
                     "modificator" : }]

        '''
        url = self.base_url + project_id + '/structures/' + str(structure_id) + '/trend'
        # url = 'http://ptsv2.com/t/10now-1539777619/post'
        data = {}
        data['period'] = period
        data['periodUnit'] = period_unit
        if segmentationNodeId:
            data['segmentationNodeId'] = segmentationNodeId
        if metric:
            data['metric'] = metric
        if custom_metric:
            data['customMetric'] = custom_metric
        if vsender_type:
            data['vsenderType'] = vsender_type

        res = self.post(url=url, data=json.dumps(data))
        return self._parse_trend_data(res, metric, period_unit, segmentationNodeId, node_label)

    def _parse_trend_data(self, res, metric, period_unit, segmentationNodeId, node_label):
        for item in res['values']:
            item['node_id'] = segmentationNodeId
            item['period_unit'] = period_unit
            item['metric'] = metric['metric']
            item['metric_modificator'] = metric['modificator']
            item['node_label'] = node_label
        return res['values']
