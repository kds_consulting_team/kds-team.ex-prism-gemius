'''
Created on 15. 04. 2019

@author: esner
'''
import csv
import logging
import os

from gemius import prism_client
from gemius.prism_client import Client

SAMPLED_REPORT_FILENAME = 'sampled-report'
SAMPLED_REPORT_TREND_FILENAME = 'sampled-report-trend'

REPORT_FIXED_HEADER = ['start_date', 'end_date', 'url_path', 'nodeId']
TREND_FIXED_HEADER = ['timestamp', 'value', 'node_id', 'period_unit', 'metric', 'metric_modificator', 'node_label']
REPORT_PKEY = REPORT_FIXED_HEADER
REPORT_TREND_PKEY = ['timestamp', 'node_id', 'period_unit', 'metric', 'metric_modificator']


class PrismExService(Client):

    def __init__(self, token, service_base_url=prism_client.DEFAULT_BASE):
        Client.__init__(self, token,
                        service_base_url=service_base_url)

    def get_n_save_sampled_report_node_tree(self, project_id, structure_id, start_date, end_date, node_filters,
                                            vsender_type,
                                            output_folder_path, period_filter='all', metrics=None, custom_metrics=None,
                                            include_trends=False,
                                            period_unit='day'):

        if not metrics:
            metrics = self._get_all_metrics()

        periods_dict = {"startDate": start_date,
                        "endDate": end_date,
                        "periodFilter": period_filter}

        node_filter = self._prepare_node_filter(node_filters)

        # prepare request params
        node_args = {"structure_id": structure_id,
                     "period": periods_dict,
                     "segmentationNodeId": None,
                     "metrics": metrics,
                     "vsender_type": vsender_type,
                     "custom_metrics": custom_metrics,
                     "project_id": project_id}

        # get and process root nodes
        roots = self.get_sapled_report_all_pages(**node_args)
        root_data = self._filter_branch_level(
            roots.get('data'), node_filter.get(0))

        result_files = []
        table_dest_file = None
        # get data
        if root_data:
            # build header
            data_header = roots['header']
            report_header = REPORT_FIXED_HEADER + data_header
            # get fixed data
            fixed_data = [start_date, end_date]

            table_dest_path = os.path.join(
                output_folder_path, SAMPLED_REPORT_FILENAME, 'report' + start_date + '_' + end_date)
            trend_dest_path = os.path.join(
                output_folder_path, SAMPLED_REPORT_TREND_FILENAME, 'trend' + start_date + '_' + end_date)
            # create empty directories
            if not os.path.exists(os.path.dirname(table_dest_path)):
                os.makedirs(os.path.dirname(table_dest_path))
            if not os.path.exists(os.path.dirname(trend_dest_path)):
                os.makedirs(os.path.dirname(trend_dest_path))

            with open(table_dest_path, 'w+', newline='') as table_dest_file, open(trend_dest_path, 'w+',
                                                                                  newline='') as trend_dest_file:
                writer = csv.writer(table_dest_file, delimiter=',',
                                    quotechar='"', quoting=csv.QUOTE_MINIMAL)
                writer.writerow(report_header)
                # trend table
                trend_writer = csv.DictWriter(trend_dest_file, delimiter=',',
                                              quotechar='"', quoting=csv.QUOTE_MINIMAL, fieldnames=TREND_FIXED_HEADER)
                trend_writer.writeheader()

                # iterate through root nodes and write to csv
                for root_node in root_data:
                    node_set = dict()
                    node_set[root_node['nodeId']] = root_node['label']
                    data_records = []
                    url_nodes = []
                    node_args['segmentationNodeId'] = root_node['nodeId']
                    data_records = self._save_n_iterate_branch(
                        root_data, data_records, node_filter, 1, url_nodes, fixed_data, node_args, node_set)

                    # write table branch row
                    writer.writerows(data_records)

                    # ### get and process trend data for current branch nodes
                    if include_trends:
                        # extra argument taken by trend endpoint
                        trend_args = node_args.copy()
                        trend_args['period_unit'] = period_unit
                        self._get_n_write_all_trends(node_set, trend_args, trend_writer)

            # set result files
            result_files = [{'full_path': table_dest_path,
                             'name': SAMPLED_REPORT_FILENAME,
                             'pkey': REPORT_PKEY},
                            {'full_path': trend_dest_path,
                             'name': SAMPLED_REPORT_TREND_FILENAME,
                             'pkey': REPORT_TREND_PKEY}
                            ]

        else:
            logging.warning('No data for specified period [%s - %s] and filter [%s]',
                            start_date, end_date, ','.join(node_filters))

        return result_files

    def _save_n_iterate_branch(self, branch_data, data_records, node_filter, level, url_nodes, additional_data,
                               node_params, node_set):
        for node in branch_data:

            if node.get('values'):
                if len(url_nodes) != level - 1:
                    # traversed back to node, truncate category list
                    del url_nodes[level - 1:]

                url_nodes.append(node['label'])
                value = node['values']
                node_set[node['nodeId']] = '/'.join(url_nodes)
                # set next nodeid
                node_params['segmentationNodeId'] = node['nodeId']

                data = self.get_sapled_report_all_pages(**node_params)
                curr_lvl_data = self._filter_branch_level(
                    data['data'], node_filter.get(level))

                if curr_lvl_data:
                    self._save_n_iterate_branch(
                        curr_lvl_data, data_records, node_filter, level + 1, url_nodes, additional_data, node_params,
                        node_set)
                else:
                    data_records.append(
                        additional_data + ['/'.join(url_nodes)] + [data['mainRow']['nodeId']] + value)
            else:
                raise RuntimeError(
                    'Json structure has unexpected format! [{}]'.format(data))
        return data_records

    def _filter_branch_level(self, level_data, level_filter):
        if not level_filter:
            return level_data

        return [r for r in level_data if r['label'].replace('/', '') in level_filter]

    def _prepare_node_filter(self, node_filters):
        filter_levels = {}
        for f in node_filters:
            split = f.split('/')
            for i, s in enumerate(split):
                filter_levels[i] = filter_levels.get(i, []) + [s]
        return filter_levels

    def _get_all_metrics(self):
        return [{"metric": metric, "modificator": "none"} for metric in prism_client.VALID_METRICS]

    def _get_n_write_all_trends(self, node_set, node_args, writer):
        """
        Get trends data. Currently iterates only through all standard metrics
        :param node_set:
        :param node_args:
        :return:
        """
        metrics = node_args.pop('metrics')
        # unsupported yet
        node_args.pop('custom_metrics')

        for node in node_set:
            node_args['segmentationNodeId'] = node
            node_args['node_label'] = node_set[node]
            res = self._get_trends_for_metrics(node_args, metrics)
            writer.writerows(res)

    def _get_trends_for_metrics(self, node_args, metrics):

        result_data = []
        for metric in metrics:
            node_args['metric'] = metric
            res = self.get_sampled_report_trend(**node_args)
            result_data.extend(res)

        return result_data

    def get_n_save_monitoring_report(self, output_folder_path, project_id, report_id, vsender_type, period,
                                     metrics=None, cutom_metrics=None):
        res = self.get_monitoring_report(project_id, report_id, period, vsender_type, metrics, cutom_metrics)

        data = self.parse_monitored_report(res, period)
        if len(data) == 0:
            logging.warning('Report %s does not contain any data for period %s', report_id, period)
            return []
        header = data[0].keys()

        res_file_name = 'monitoring_report_' + report_id
        rep_dest_path = os.path.join(
            output_folder_path, res_file_name, 'mon_report_' + period + '_' + project_id)
        # create empty directories
        if not os.path.exists(os.path.dirname(rep_dest_path)):
            os.makedirs(os.path.dirname(rep_dest_path))

        # write data
        with open(rep_dest_path, 'w+', newline='') as table_dest_file:
            writer = csv.DictWriter(table_dest_file, delimiter=',',
                                    quotechar='"', quoting=csv.QUOTE_MINIMAL, fieldnames=header)
            writer.writeheader()
            writer.writerows(data)

        result_files = [{'full_path': rep_dest_path,
                         'name': res_file_name,
                         'pkey': ['guaranteed_tu', 'period', 'node_id']}]

        return result_files

    def parse_monitored_report(self, resp, period):
        end_timestamp = resp['guaranteedTU']
        root_node_id = resp['rootNodeId']
        header = resp['definitions']
        items = resp['values']
        for item in items:
            item['guaranteed_tu'] = end_timestamp
            item['root_node_id'] = root_node_id
            item['period'] = period
            item['node_id'] = item.pop('id')
            # insert values
            for i, val in enumerate(item['values']):
                item[header[i]] = val
            # remove values object
            item.pop('values')
        return items
